<div class="row">
    <div class="col-lg-12">
<section class="panel">
    <header class="panel-heading">
دفترچه تلفن    </header>
    <table class="table table-striped table-advance table-hover">

        <thead>
        <tr>
            <td>نام</td>
            <td>نام خانوادگی</td>
            <td>تلغن همراه</td>
            <td>آدرس</td>
            <td>ویرایش</td>
            <td>حذف</td>
        </tr>
        </thead>

        <tbody>

<?php 
include_once'app/contact.php';
$obj=new contact();
$obj->setTbl('contact_tbl');
$result=$obj->list_contact();


foreach ($result as $value) :
    ?>

        <tr>
            <td><?= $value->name ?></td>
            <td><?= $value->lastname ?> </td>
            <td><?= $value->tel ?></td>
            <td><?= $value->addr ?></td>
            <td><a href="dashbord.php?contact=edit&id=<?= $value->id ?>" class="btn btn-primary btn-xs"> <i class="icon-pencil"></i></a></td>
            <td><a href="dashbord.php?contact=delete&id=<?= $value->id ?>" class="btn btn-primary btn-xs"> <i class="icon-trash"></i></a></td>
        </tr>
<?php
endforeach; 
?>
        </tbody>
    </table>
</section>

    </div>


</div>