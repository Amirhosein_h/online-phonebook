
<?php 
include_once'app/contact.php';
$id=$_GET['id'];
$obj=new contact();
$obj->setTbl('contact_tbl');
$result=$obj->showEditData($id);


if (isset($_POST['btn'])) {
    $data=$_POST['frm'];
    $filds=array_keys($data);
    $obj->editData($filds, $data, $id);
}
?>
<h1 class="pageLables">افزودن شماره تماس جدید</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2">
        <section class="panel">
            <header class="panel-heading">
           افزودن شماره تماس جدید
            </header>

            <div class="panel-body">
                <form role="form" method="post">
                    <div class="form-group">
                        <lable for="exampleInputEmail">  نام</lable>
                        <input type="text" name="frm[name]" class="form-control"  placeholder=" نام خود را وارد کنید" value="<?= $result['name']; ?>" >
                    </div>

                    <div class="form-group">
                        <lable for="exampleInputPassword1"> نام خانوادگی </lable>
                        <input type="text" name="frm[lastname]" class="form-control"  placeholder="نام خانوادگی خود را وارد کنید " value="<?= $result['lastname']; ?>">
                    </div>

                    <div class="form-group">
                        <lable for="exampleInputPassword1"> شماره تماس </lable>
                        <input type="text" name="frm[tel]" class="form-control"  placeholder="شماره تماس خود را وارد کنید "value="<?= $result['tel']; ?>">
                    </div>

                    
                    <div class="form-group">
                        <lable for="exampleInputPassword1"> آدرس</lable>
                        <input type="text" name="frm[addr]" class="form-control"  placeholder="آدرس خود را وارد کنید " value="<?= $result['addr']; ?>">
                    </div>

                    <button name="btn" type="submit" class="btn btn-info">ویرایش </button>

                </form>
            </div>
        </section>
    </div>
</div>
